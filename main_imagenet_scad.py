from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function


import argparse
import os
import sys
import shutil
import time
import logging
import json
import glob
import re
import numpy as np
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets

from utils_scad import *
from layers import *
from model_imagenet_resnet import *
from datetime import date

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')

parser.add_argument('--model', default='r50', type=str, metavar='MODEL',
                    help='model architecture: ?? (default: r50)')
parser.add_argument('-j', '--workers', default=16, type=int, metavar='N',
                    help='number of data loading workers (default: 16)')
parser.add_argument('--epochs', default=90, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('--decay-epochs', default=30, type=int, metavar='N',
                    help='number of epochs to decay the learning rate')
parser.add_argument('-b', '--batch-size', default=256, type=int,
                    metavar='N', help='mini-batch size (default: 256)')
parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--start-lr',  default=0.01, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
# parser.add_argument('--weight-decay', '--wd', default=0., type=float,
#                     metavar='W', help='weight decay (default: 0.)')
parser.add_argument('--print-freq', '-p', default=100, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')

parser.add_argument('--seed', default=1234, type=int, help='random seed (default: 1234)')
parser.add_argument('--best-path', default=1234, type=int, help='what checkpoint')

parser.add_argument('--world-size', dest='world_size', default=10.0, type=float,
                    help='use pre-trained model')

parser.add_argument('--optimizer', default="SGD", type=str, help='optimizer')
parser.add_argument('--cwd', default="00", type=str, help='path dir')
parser.add_argument('--datename', default="00", type=str, help='path name')

parser.add_argument('--l2_reg', default=1e-4,type=float)
parser.add_argument('--l1_reg', default=0., type=float)
parser.add_argument('--l2_regreal', default=0, type=float)
parser.add_argument('--l1_regreal', default=0, type=float)
parser.add_argument('--compression', default=0.1, type=float)
parser.add_argument('--pattern', default='uniform', type=str)
parser.add_argument('--linear', default=1, type=int, help='linear')
parser.add_argument('--linear-per', default=0.5, type=float)
best_prec1 = 0
FORMAT = '[%(levelname)s: %(filename)s: %(lineno)4d]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
logger = logging.getLogger(__name__)

# args = parser.parse_args()
def main():

    global args, best_prec1
    args = parser.parse_args()
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(args.seed)
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    path_imagetnet = "./imagenet_full_size/"
    traindir = os.path.join(path_imagetnet, 'train')
    valdir = os.path.join(path_imagetnet, 'val')
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    train_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(traindir, transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=True,
        num_workers=args.workers, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(valdir, transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    models = {
        # 'r18': resnet18,
        'r18': resnet18,
        'r34': resnet34,
        'r50': resnet50,
        'wr18-3': wresnet18x3e,
        'wr34-3': wresnet34x3e,
        'wr18-6': wresnet18x6e,
        'wr34-6': wresnet34x6e,
        'wr50-2': wresnet50x2e,
        'wr50-3': wresnet50x3e,
        # 'r101': resnet101,
    }
    if args.pretrained:
        print("=> using pre-trained model '{}'".format(args.model))
        model = models[args.model](pretrained=True)
    else:
        if args.model == 'rx50':
            model = models[args.model](4,32)
        else:
            model = models[args.model]()
    print (model)
    model = torch.nn.DataParallel(model).cuda()
    # define loss function (criterion) and optimizer
    criterion = nn.CrossEntropyLoss().cuda()
    optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                momentum=args.momentum,
                                weight_decay=args.l2_regreal)
    # creat path
    path_cpt_first = './'+args.cwd+'/'+args.datename+'/'
    cwd = os.getcwd()
    path_cpt = cwd+'/'+args.datename+'/'
    if not os.path.exists(path_cpt):
        os.makedirs(path_cpt)
        print ("No exist, creating following path")
        print (path_cpt)
    else:
        print ("already exist")
        print (path_cpt)


    files = os.listdir(path_cpt)
    files_txt = [i for i in files if i.endswith('checkpoint_fine_tune.pth.tar')]
    if len(files_txt)==1:
        checkpoint_path = path_cpt + files_txt[0]
    else:
        print ("looking in to following path to find checkpoint")
        print (path_cpt_first)
        files = os.listdir(path_cpt_first)
        if args.best_path==1:
            files_txt = [i for i in files if i.endswith('best.pth.tar')]
            print ("old checkpoint", files_txt)
            if len(files_txt)==1:
                checkpoint_path = path_cpt_first+files_txt[0]
            elif len(files_txt)>1:
                find_list = []
                for string in files_txt:
                    x = re.findall(r'\d+_best',string)
                    try:
                        find_list.append(int(x[0].split('_best')[0]))
                    except Exception:
                        pass
                try:
                    print ("_epoch_"+str(max(find_list))+"_best.pth.tar")
                    standard = 'LoneREG_'+str(args.l1_reg)+'_LtwoREG_'+str(args.l2_reg)+"_"+args.model
                    checkpoint_path = path_cpt_first+standard+"_epoch_"+str(max(find_list))+"_best.pth.tar"
                except Exception:
                    print ("NO Found CheckPoint from old checkpoint directory")
            else:
                print ("NO Found CheckPoint")
                checkpoint_path = path_cpt+'sssssssss'
        else:
            #files_txt = [i for i in files if i.endswith('checkpoint_fine_tune.pth.tar')]
            files_txt = [i for i in files if i.endswith('checkpoint.pth.tar')]
            if len(files_txt)==1:
                checkpoint_path = path_cpt_first +files_txt[0]
            else:
                print ("NO Found CheckPoint")
                checkpoint_path = path_cpt+'sssssssss'

    if os.path.isfile(checkpoint_path):
        print("=> loading checkpoint '{}'".format(checkpoint_path))
        checkpoint = torch.load(checkpoint_path)
        if checkpoint_path.endswith('checkpoint_fine_tune.pth.tar'):
            args.start_epoch = checkpoint['epoch']
            optimizer.load_state_dict(checkpoint['optimizer'])
            #args.epochs = args.start_epoch+15
            if args.start_epoch<=30:
                lr = 0.1
            elif args.start_epoch>30 and args.start_epoch<=50:
                lr = 0.01
            elif args.start_epoch>50 and args.start_epoch<=80:
                lr = 0.001
            elif args.start_epoch>80:
                lr = 0.0001
            for param_group in optimizer.param_groups:
                param_group['lr'] = lr
        else:
            args.start_epoch = 0
        best_prec1 = checkpoint['best_prec1']
        model.load_state_dict(checkpoint['state_dict'])

        print("=> loaded checkpoint '{}' (epoch {})"
              .format(args.resume, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(checkpoint_path))

    print ("What is the model?",args.model)
    print ("What is the batch-size?", args.batch_size)
    print ("Regulization L1 is %s and L2 is %s?"%(str(args.l1_reg),str(args.l2_reg)))
    cudnn.benchmark = True
    ###################Start Prune###########################################################################


    #test_acc0 = validate(val_loader, model, criterion)
    test_loss_prune0, prec1_prune0, prec5_prune0 = validate(val_loader,model,criterion,ignore=True)

    #############################################################################################################################
    compression, masks, model = compressor_prune_scad(args.compression,model,printout=True)
    ##############################################################################################################################
    test_loss_prune1, prec1_prune1, prec5_prune1 = validate(val_loader,model,criterion,ignore=True)


    try:
        checkpoint_epoch = checkpoint['epoch']
    except Exception:
        checkpoint_epoch = -1
    json_stats = dict(
        epoch_at_prune = checkpoint_epoch,
        test_loss_a='0',
        test_acc_a='0',
        test_acc5_a='0',
        test_loss_b='0',
        test_acc_b='0',
        test_acc5_b='0',
        l1reg = args.l1_reg,
        l2reg = args.l2_reg,
        compression = '0',
        l1regreal = args.l1_regreal,
        l2regreal = args.l2_regreal,
    )
    json_stats['test_loss_a'] = test_loss_prune0
    json_stats['test_acc_a'] = prec1_prune0
    json_stats['test_acc5_a'] = prec5_prune0
    json_stats['test_loss_b'] = test_loss_prune1
    json_stats['test_acc_b'] = prec1_prune1
    json_stats['test_acc5_b'] = prec5_prune1
    json_stats['compression'] = (compression).item()

    json_stats.update()
    log_json_stats(json_stats)
    save_json(json_stats, path_cpt+'LoneREG_'+str(args.l1_reg)+'_LtwoREG_'+str(args.l2_reg)+"_"+args.model+'_json_stats.log')

###################Finish Prune###########################################################################
###################Start Fine-Tune###########################################################################

    json_stats = dict(
        epoch = 0,
        train_loss='0',
        train_acc= 0.,
        test_loss='0',
        test_acc='0',
        test_acc5='0',
        num_prune = '0',
    )

    for epoch in range(args.start_epoch, args.epochs):
        #adjust_learning_rate(optimizer, epoch, args)

        #####################################################################################################
        zero_parameters = get_conv_zero_param(model)
        print('Zero parameters: {}'.format(zero_parameters))
        #####################################################################################################

        # train for one epoch
        train_loss, train_top1, train_top5, masks = train(train_loader, model, criterion, optimizer, epoch, masks)
        for param_group in optimizer.param_groups:
            lr = param_group['lr']
        # evaluate on validation set
        test_loss, prec1, prec5 = validate(val_loader, model, criterion)

        # remember best prec@1 and save checkpoint
        is_best = prec1 > best_prec1
        best_prec1 = max(prec1, best_prec1)

        print ('\nEpoch: %d with lr rate %.5f' % (epoch, lr))
        print ('Train Loss: %.3f |Train Acc: %.3f |Top5: %.3f ' %(train_loss, train_top1, train_top5))
        print ('Test Loss: %.3f |Test Acc Top1: %.3f |Top5: %.3f '% (test_loss, prec1, prec5))
        print ('\n')
        json_stats['epoch'] = epoch
        json_stats['train_loss'] = train_loss
        json_stats['train_acc'] = train_top1
        json_stats['test_loss'] = test_loss
        json_stats['test_acc'] = prec1
        json_stats['test_acc5'] = prec5
        json_stats['num_prune'] = zero_parameters.item()
        json_stats.update()

        log_json_stats(json_stats)
        save_json(json_stats, path_cpt+'LoneREG_'+str(args.l1_reg)+'_LtwoREG_'+str(args.l2_reg)+"_"+args.model+'_json_stats.log')
        if epoch>=20 and epoch%3 == 2:
            save_checkpoint(epoch,{
                'epoch': epoch + 1,
                'arch': args.model,
                'state_dict': model.state_dict(),
                'best_prec1': best_prec1,
                'optimizer' : optimizer.state_dict(),
            }, is_best,path_check = path_cpt+'LoneREG_'+str(args.l1_reg)+'_LtwoREG_'+str(args.l2_reg)+"_"+args.model,name = '_fine_tune')
        elif epoch%5 == 4 and epoch<20:
            save_checkpoint(epoch,{
                'epoch': epoch + 1,
                'arch': args.model,
                'state_dict': model.state_dict(),
                'best_prec1': best_prec1,
                'optimizer' : optimizer.state_dict(),
            }, is_best, path_check = path_cpt+'LoneREG_'+str(args.l1_reg)+'_LtwoREG_'+str(args.l2_reg)+"_"+args.model,name = '_fine_tune')

def get_conv_zero_param(model):
    total = 0
    for m in model.modules():
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            total += torch.sum(m.weight.data.eq(0))
    return total


def train(train_loader, model, criterion, optimizer, epoch, mask_dic):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to train mode
    model.train()
    if args.optimizer != 'SGD_warmup':
        adjust_learning_rate(optimizer, epoch, args)

    end = time.time()
    for i, (inputs, targets) in enumerate(train_loader):
        # measure data loading time
        if args.optimizer == 'SGD_warmup':
            momentum_scale, lr = warmup_learning_rate(optimizer, epoch, len(train_loader),i)

        data_time.update(time.time() - end)

        inputs, targets = inputs.cuda(), targets.cuda()
        input_var = torch.autograd.Variable(inputs)
        target_var = torch.autograd.Variable(targets)
        # compute output
        outputs = model(input_var)
        loss = criterion(outputs, target_var)

        prec1, prec5 = accuracy(outputs.data, targets, topk=(1, 5))

        losses.update(loss.item(), inputs.size(0))
        top1.update(prec1.item(), inputs.size(0))
        top5.update(prec5.item(), inputs.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        if args.l1_regreal > 0.:
            loss += args.l1_regreal * lp_regularization(model, p=1)


        loss.backward()
#######Zero Out the update of the prune parameters######################################################################
        msk = 0
        for k, m in enumerate(model.modules()):
            # print(k, m)
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
                # weight_copy = m.weight.data.abs().clone()
                # mask = weight_copy.gt(0).float().cuda()
                m.weight.grad.data.mul_(mask_dic[msk])
                msk += 1
            # elif isinstance(m, nn.Linear) and args.linear==1 :
            #     m.weight.grad.data.mul_(mask_dic[msk])
            #     msk += 1
#############################################################################
        if args.optimizer == 'SGD_warmup':
            optimizer.step(scale=momentum_scale)
            momentum_scale=None
        else:
            optimizer.step()
        # optimizer.step()
        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()
        if i % args.print_freq == 0:
            compression, masks, model = compressor_prune_scad(args.compression, model, printout=False)
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})\t'
                  .format(epoch, i, len(train_loader), batch_time=batch_time,
                   data_time=data_time,top1=top1, top5=top5))
    # return top1.avg
    # print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'.format(top1=top1, top5=top5))
    # return losses.avg,top1.avg,top5.avg
    return  losses.avg, top1.avg, top5.avg, masks

def validate(val_loader, model, criterion, ignore=False):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()
    compression, masks, model = compressor_prune(args.compression,model)
    print ("compresssion rate:",compression.item())
    # switch to evaluate mode
    model.eval()
    with torch.no_grad():
        end = time.time()
        for i, (inputs, targets) in enumerate(val_loader):
            inputs, targets = inputs.cuda(), targets.cuda()
            input_var = torch.autograd.Variable(inputs)
            target_var = torch.autograd.Variable(targets)
            # compute output

            outputs = model(input_var)
            loss = criterion(outputs, target_var)
            # measure accuracy and record loss
            prec1,prec5 = accuracy(outputs.data, targets, topk=(1,5 ))
            losses.update(loss.item(), inputs.size(0))
            top1.update(prec1.item(), inputs.size(0))
            top5.update(prec5.item(), inputs.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()
            if not ignore:
                if i % args.print_freq == 0:
                    print('Test: [{0}/{1}]\t'
                          'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                          'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                          'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                           i, len(val_loader), batch_time=batch_time,
                           top1=top1, top5=top5))

    # return top1.avg
    print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'.format(top1=top1, top5=top5))
    return losses.avg,top1.avg,top5.avg


def save_json(json_stats,filename):
    with open(filename, 'a') as fopen:
        fopen.write(json.dumps(json_stats, sort_keys=False))
        fopen.write("\n")

def log_json_stats(stats):
    print('\njson_stats: {:s}\n'.format(json.dumps(stats, sort_keys=False)))
    return None


def save_checkpoint(epoch, state, is_best, path_check='', name = ''):
    torch.save(state, path_check+ 'checkpoint'+name+'.pth.tar')
    if epoch>70 and is_best:
        shutil.copyfile(path_check+ 'checkpoint'+name+'.pth.tar',  path_check+'best.pth.tar')

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def adjust_learning_rate(optimizer, epoch, args):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    # if epoch %30 == 29 or epoch == 59 or  epoch == 79:
    if epoch == 30 or epoch == 50 or epoch == 80:
        for param_group in optimizer.param_groups:
            lr = param_group['lr']
            param_group['lr'] = 0.1*lr



def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res

#def adjust_learning_rate(optimizer, epoch):
def warmup_learning_rate(optimizer, epoch,num_iter_in_one_epoch,iter_index):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    #lr = args.lr * (0.1 ** (epoch // 30))
    if epoch < args.start_epoch + 10:
        world_size = (args.lr *1. /args.start_lr )
        lr_step = (world_size  - 1) * args.start_lr / (10.0 * num_iter_in_one_epoch)
        lr = args.start_lr + ((epoch-args.start_epoch) * num_iter_in_one_epoch + iter_index) * lr_step
        # wd = args.weight_decay
    else:
        lr = args.lr
    # elif epoch <=  args.start_epoch + 15:#90:
    #     lr = args.lr *(0.1 ** (epoch//args.decay_epochs))
    # else:
    #     lr = args.lr * (0.1**3)
    scale = None
    for param_group in optimizer.param_groups:
        lr_prev = param_group['lr']
        param_group['lr'] = lr
        if lr > lr_prev:
            scale = lr / lr_prev
    return scale,lr



if __name__ == '__main__':
    main()
