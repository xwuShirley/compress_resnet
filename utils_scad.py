import torch
import torch.nn as nn

import os, string, random, time

import numpy as np

def prune_layer(w, s=0.1):
    retain_top_p(w.data.flatten(), p=s)

def prune_resnet(m, pattern):
    for k, p in m.named_parameters():
        for K, S in pattern.items():
            if k.startswith(K) and 'bn' not in k:
                prune_layer(p, s=S)

def adjust_learning_rate(initial_lr, optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = initial_lr * (0.1 ** (epoch // 50))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def count_parameters(model):
    return sum(p.numel() for p in model.parameters())

def compressor_prune_scad(compression,model,printout=False):
    compression_up = {0.3:0.4,
    0.179:0.3,
    0.079:0.158,
    0.0413:0.0826,
    0.03:0.06,
    0.0283:0.0566,
    0.0273:0.0546,
    0.023:0.046,
    }
    compression_mid = {0.3:0.35,
    0.179:0.2,
    0.079:0.1,
    0.0413:0.0626,
    0.03:0.045,
    0.0283:0.0426,
    0.0273:0.041,
    0.023:0.036,

    }
    total = 0
    for m in model.modules():
        if  isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            total += m.weight.data.numel()
    weights = torch.zeros( total).cuda()
    index = 0
    for m in model.modules():
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            size = m.weight.data.numel()
            weights[index:(index+size)] = m.weight.data.view(-1).abs().clone()
            index += size
    y, i = torch.sort(weights)

    thre_index = int(total * (1-compression))
    threshold  = y[thre_index]

    thre_index = int(total * (1-compression_up[compression]))
    threshold_up  = y[thre_index]


    thre_index = int(total * (1-compression_mid[compression]))
    threshold_mid  = y[thre_index]


############################################
    zero_flag = 0
    total = 0
    pruned = 0
    masks = []

    for k, m in enumerate(model.modules()):
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
#             print ("AT FIRST")
#             print (m.weight.data)
#             print ("=============================")
            weight_copy = m.weight.data.abs().clone()

            mask = weight_copy.gt(threshold).float().cuda()
            mask_up = weight_copy.gt(threshold_up).float().cuda()
            mask_mid = weight_copy.gt(threshold_mid).float().cuda()
#             print (threshold,threshold_up,threshold_mid)

            # this part is for soft_threshold
#             print(mask_up)
#             print (mask_mid )
#             print (mask)
            mask_decrease = mask_up - mask_mid
#             print (mask_decrease)
            weight_soft = (weight_copy-threshold_up).mul_(m.weight.data.sign()).mul_(mask_decrease)
#             print ('weight_soft', weight_soft)



            # this part is for soft_threshold
            mask_linear_increase = mask_mid - mask
            weight_linear =  (weight_copy-threshold).mul(1.+ threshold_up/(threshold-threshold_mid))+threshold
            weight_linear = weight_linear.mul_(m.weight.data.sign()).mul_(mask_linear_increase)

#             print ('weight_linear',weight_linear)

#             print (mask_up)
#             print (print ("AT LAST"))
#             print ("++++++++++++++++++++++++++++++")
            m.weight.data.mul_(mask).add_(weight_linear).add_(weight_soft)
            masks.append(mask_up)
            total = mask.numel()
            remain = torch.sum(mask_up)
            pruned += total-remain
            if int(torch.sum(mask_up)) == 0:
                zero_flag += 1
            if printout:
                print('index{}\t{} \t total params: {:d} \t remaining params: {:3f}'.
                    format(k,str(m)[0:15],total, remain/total))


    total = count_parameters(model)
    if zero_flag>0:
        print ("There exists layer with %s parameters left."%(str(zero_flag)))
    if printout:
        print('Pruning threshold: {}'.format(threshold))
        print(' Pruned params: {}, Pruned ratio: {}'.format(pruned, pruned/total))

    return (total-pruned)/total, masks, model


def compressor_prune(compression,model,printout=False):
    total = 0
    for m in model.modules():
        if  isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):#or isinstance(m, nn.BatchNorm2d):# or
            total += m.weight.data.numel()

    # create a huge weight matrix "conv_weights" for all the weight and pass the model weights
    weights = torch.zeros( total).cuda()
    index = 0
    for m in model.modules() or isinstance(m, nn.Linear):
        if isinstance(m, nn.Conv2d): #or isinstance(m, nn.BatchNorm2d):## isinstance(m, nn.BatchNorm2d) or
            size = m.weight.data.numel()
            weights[index:(index+size)] = m.weight.data.view(-1).abs().clone()
            index += size

    # sort the weights and take truncate the weights according to the threshold
    y, i = torch.sort(weights)

    thre_index = int(total * (1-compression))
    threshold  = y[thre_index]
############################################
    zero_flag = 0
    total = 0
    pruned = 0
    masks = []
    for k, m in enumerate(model.modules()):
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            weight_copy = m.weight.data.abs().clone()
            mask = weight_copy.gt(threshold).float().cuda()
            m.weight.data.mul_(mask)
            masks.append(mask)
            total = mask.numel()
            remain = torch.sum(mask)
            pruned += total-remain
            if int(torch.sum(mask)) == 0:
                zero_flag += 1

            if printout:
                print('index{}\t{} \t total params: {:d} \t remaining params: {:3f}'.
                    format(k,str(m)[0:15],total, remain/total))


    total = count_parameters(model)
    if zero_flag>0:
        print ("There exists layer with %s parameters left."%(str(zero_flag)))
    if printout:
        print('Pruning threshold: {}'.format(threshold))
        print(' Pruned params: {}, Pruned ratio: {}'.format(pruned, pruned/total))

    return (total-pruned)/total, masks, model
